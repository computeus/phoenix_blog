defmodule Blog.Plugs.BlogInfoSetter do
  import Plug.Conn
  alias Blog.Repo
  alias Blog.BlogInfo

  def init(default), do: default

  def call(conn, _default) do
    conn = assign(conn, :blog_title, "asd")
    conn = assign(conn, :blog_slogan, "zxc")
    conn = assign(conn, :subdomain, "")

    subdomain = get_subdomain(conn.host)

    if byte_size(subdomain) > 0 do
      current_blog = Repo.get_by(BlogInfo, subdomain: subdomain)

      if current_blog do
        conn = assign(conn, :blog_title, current_blog.title)
        conn = assign(conn, :blog_slogan, current_blog.slogan)
        conn = assign(conn, :current_blog, current_blog)
      else
        Phoenix.Controller.redirect(conn, external: "http://#{Blog.Endpoint.config(:url)[:host]}:4000?flash[error]=blog_not_found")
      end
    else
      subdomain = nil
    end
    conn = assign(conn, :subdomain, subdomain)

    conn
  end

  defp get_subdomain(host) do
    root_host = Blog.Endpoint.config(:url)[:host]
    String.replace(host, ~r/.?#{root_host}/, "")
  end
end
