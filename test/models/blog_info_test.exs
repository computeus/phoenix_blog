defmodule Blog.BlogInfoTest do
  use Blog.ModelCase

  alias Blog.BlogInfo

  @valid_attrs %{slogan: "some content", subdomain: "some content", title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = BlogInfo.changeset(%BlogInfo{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = BlogInfo.changeset(%BlogInfo{}, @invalid_attrs)
    refute changeset.valid?
  end
end
