defmodule Blog.Repo.Migrations.CreateBlogInfo do
  use Ecto.Migration

  def change do
    create table(:blog_infos) do
      add :title, :string
      add :slogan, :string
      add :subdomain, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create index(:blog_infos, [:user_id])

  end
end
