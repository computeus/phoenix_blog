defmodule Blog.PageController do
  use Blog.Web, :controller

  def index(conn, _params) do
    if conn.assigns[:subdomain] do
      posts = Blog.PostController.set_index_variables(conn)
      render(conn, Blog.PostView, :index, posts: posts)
    else
      render conn, :index, layout: { Blog.LayoutView, "landing.html" }
    end
  end
end
