defmodule Blog.CommentController do
  use Blog.Web, :controller

  alias Blog.Comment
  alias Blog.Post

  def new(conn, %{"post_id" => post_id}) do
    post = Repo.get!(Post, post_id)
    changeset = Comment.changeset(%Comment{})
    render(conn, "new.html", changeset: changeset, post: post)
  end

  def create(conn, %{"comment" => comment_params, "post_id" => post_id}) do
    post = Repo.get!(Post, post_id)

    changeset =
      %Comment{}
        |> Comment.changeset(comment_params)
        |> Ecto.Changeset.put_assoc(:post, post)

    case Repo.insert(changeset) do
      {:ok, _comment} ->
        conn
        |> put_flash(:info, "Comment created successfully.")
        |> redirect(to: post_path(conn, :show, post))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, post: post)
    end
  end

  def edit(conn, %{"id" => id, "post_id" => post_id}) do
    post = Repo.get!(Post, post_id)
    comment = Repo.get!(Comment, id)
    changeset = Comment.changeset(comment)
    render(conn, "edit.html", comment: comment, changeset: changeset, post: post)
  end

  def update(conn, %{"id" => id, "comment" => comment_params}) do
    comment = Repo.get!(Comment, id) |> Repo.preload(:post)
    changeset = Comment.changeset(comment, comment_params)

    case Repo.update(changeset) do
      {:ok, comment} ->
        conn
        |> put_flash(:info, "Comment updated successfully.")
        |> redirect(to: post_path(conn, :show, comment.post))
      {:error, changeset} ->
        render(conn, "edit.html", comment: comment, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    comment = Repo.get!(Comment, id)
    post = comment.post

    Repo.delete!(comment)

    conn
    |> put_flash(:info, "Comment deleted successfully.")
    |> redirect(to: post_path(conn, :index, post))
  end
end
