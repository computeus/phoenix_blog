defmodule Blog.Comment do
  use Blog.Web, :model

  schema "comments" do
    field :body, :string
    belongs_to :user, Blog.User
    belongs_to :post, Blog.Post

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:body])
    |> validate_required([:body])
    |> validate_length(:body, min: 5)
    |> validate_length(:body, max: 255)
    |> assoc_constraint(:post)
    |> assoc_constraint(:user)
  end
end
