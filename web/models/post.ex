defmodule Blog.Post do
  use Blog.Web, :model

  schema "posts" do
    field :title, :string
    field :body, :string
    field :is_draft, :boolean, default: false
    field :publish_at, :naive_datetime
    belongs_to :user, Blog.User
    has_many :comments, Blog.Comment

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :body, :is_draft, :publish_at])
    |> set_publish_at
    |> validations
  end

  defp set_publish_at(changeset) do
    case fetch_field(changeset, :publish_at) do
      {:data, nil} ->
        put_change(changeset, :publish_at, Ecto.DateTime.utc)
      _ ->
        changeset
    end
  end

  defp validations(changeset) do
    changeset
    |> validate_required([:title, :body, :user_id])
    |> validate_length(:title, min: 5)
    |> validate_length(:title, max: 255)
    |> validate_length(:body, min: 5)
    |> validate_length(:body, max: 100000000)
    |> validate_length(:title, max: 255)
    |> assoc_constraint(:user)
  end
end
