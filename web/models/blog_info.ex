defmodule Blog.BlogInfo do
  use Blog.Web, :model

  schema "blog_infos" do
    field :title, :string
    field :slogan, :string
    field :subdomain, :string
    belongs_to :user, Blog.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :slogan, :subdomain])
    |> validate_required([:title, :slogan, :subdomain])
  end
end
